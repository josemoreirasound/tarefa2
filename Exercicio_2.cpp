#include <iostream>
//#include <stdlib.h> 
//#include <stdio.h>
#include <math.h>

using namespace std;

int main()
{
	double a = 0.0, b= 0.0, c = 0.0;
	double x1 = 0.0, x2 = 0.0, M, MM, MM1;
	char teste;
	do {
	cout << "Digite os coeficientes da equacao " << endl;
	cout << "  " << endl;
	cout << "Primeiro o coeficiente quadratico " << endl;
	cin >> a;
	cout << "Digite o coeficiente de primeiro grau " << endl;
	cin >> b;
	cout << "Digite o coeficiente de grau zero " << endl;
	cin >> c;
	M = ((b * b) - (4* a * c));
	
	if(M >= 0){
		x1 = pow(M, 0.5) - b;
		x2 = - pow(M, 0.5) - b;
		cout << "As raizes da equacao sao : " << endl;
		cout << x1/2 << "  e  " << x2/2 << endl;
		
	}
	else{
		MM = - M;
		MM1 = (pow(MM, 0.5));
		cout << "As raizes, complexas, da equacao sao : " << endl;
		cout << - b/2 <<" + " << MM1/2 << "i" << endl;
		cout << - b/2 << " - " << MM1/2 << "i"  << endl;
	}		
	cout << "Deseja calcular uma nova equacao ? " << endl;
	cout << "s para sim e n para nao " << endl;
	cin >> teste;		
	}	
	
	while(teste == 's');
	
	system("pause");
	
	return 0;
}
